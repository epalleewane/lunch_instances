resource "aws_security_group" "WebDMZ-tf" {
  name        = "WebDMZ-tf"
  description = "Allow rdp traffic"


  ingress {

    from_port   = 22
    to_port     = 22
    protocol =   "tcp"

    cidr_blocks =  ["0.0.0.0/0"]
  }
}