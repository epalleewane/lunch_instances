resource "aws_instance" "linux_vm" {
  ami             = "ami-2757f631"
  instance_type   = "t2.micro"
  key_name        = "puppetkey"
  security_groups = ["${aws_security_group.WebDMZ-tf.name}"]
tags      = {
     Name = "Application"
     Timeflag = "24x7"
     apptype  = "Activedirectory"
 }

}